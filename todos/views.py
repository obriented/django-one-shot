from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/todolist.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/todolist_detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/todolist_new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/todolist_edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/todolist_delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/todoitem_new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/todoitem_edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
